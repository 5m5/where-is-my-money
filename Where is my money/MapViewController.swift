//
//  MapViewController.swift
//  Where is my money
//
//  Created by Mikhail Andreev on 19/05/2018.
//  Copyright © 2018 Mikhail Andreev. All rights reserved.
//

import UIKit
import RealmSwift
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate {
    
    public func sum<T: AddableType>(ofProperty property: String) -> T? {
        return records.sum(ofProperty: property)
    }
    
    //var pin: AnnotationPin!
    
    let realm = try! Realm()
    lazy var records: Results<RecordModel> = { self.realm.objects(RecordModel.self) }()
    
    func countRecords() -> Int {
        var count = 0
        for i in 0 ..< records.count {
            if records[i].latitude != 0 {
                count += 1
            }
        }
        return count
    }
    
    func getLatitudeSummValue() -> CLLocationDegrees {
        return sum(ofProperty: "latitude")!
    }
    
    func getLatitudeAverageValue() -> CLLocationDegrees {
        let count = CLLocationDegrees(countRecords())
        if count == 0 {
            return 0
        }
        return (getLatitudeSummValue())/count
    }
    
    func getLongitudeSummValue() -> CLLocationDegrees {
        return sum(ofProperty: "longitude")!
    }
    
    func getLongitudeAverageValue() -> CLLocationDegrees {
        let count = CLLocationDegrees(countRecords())
        if count == 0 {
            return 0
        }
        return (getLongitudeSummValue())/count
    }
    
//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//        guard !(annotation is MKUserLocation) else { return nil }
//
//        let annotationIdentifier = "Annotation"
//        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) as? MKPinAnnotationView
//
//        if annotationView == nil {
//            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier) as? MKPinAnnotationView
//            annotationView?.canShowCallout = true
//        }
//
//        let rightImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
//        rightImage.image = UIImage(named: records[0].selectedCategory.icon)
//        annotationView?.rightCalloutAccessoryView = rightImage
//
//        //annotationView?.pinTintColor = .blue
//        return annotationView
//    }
    
//    func addAnnotations() {
//        let coordinate: CLLocationCoordinate2D = CLLocationCoordinate2DMake(getLatitudeAverageValue(), getLongitudeAverageValue())
//        let span = MKCoordinateSpanMake(1, 1)
//        let region = MKCoordinateRegionMake(coordinate, span)
//        self.mapView.setRegion(region, animated: true)
//
//        for i in 0 ..< records.count {
//            if records[i].latitude != 0 {
//                let annotation = MKPointAnnotation()
//                annotation.title = records[i].selectedCategory.name
//                annotation.coordinate = CLLocationCoordinate2DMake(records[i].latitude, records[i].longitude)
//
////                pin = AnnotationPin(title: "TMP", subtitle: "TMP", coordinate: coordinate)
////                pin.coordinate = CLLocationCoordinate2DMake(records[i].latitude, records[i].longitude)
//                self.mapView.addAnnotation(annotation)
//            }
//        }
//    }
    
    func addAnnotations() {
        let coordinate: CLLocationCoordinate2D = CLLocationCoordinate2DMake(getLatitudeAverageValue(), getLongitudeAverageValue())
        let span = MKCoordinateSpanMake(1, 1)
        let region = MKCoordinateRegionMake(coordinate, span)
        self.mapView.setRegion(region, animated: true)
        
        for i in 0 ..< records.count {
            if records[i].latitude != 0 {
                let annotation = CustomPointAnnotation()
//                annotation.title = records[i].selectedCategory.name
                annotation.imageName = records[i].selectedCategory.icon
                annotation.coordinate = CLLocationCoordinate2DMake(records[i].latitude, records[i].longitude)
                self.mapView.addAnnotation(annotation)
            }
        }
    }

    @IBOutlet weak var mapView: MKMapView!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        addAnnotations()
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if !(annotation is CustomPointAnnotation) {
            return nil
        }
        
        let reuseId = "annotation"
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            annotationView?.canShowCallout = true
        } else {
            annotationView?.annotation = annotation
        }
        
        //Set annotation-specific properties **AFTER**
        //the view is dequeued or created...
        
        let cpa = annotation as! CustomPointAnnotation
        annotationView?.image = UIImage(named:cpa.imageName)
        
        let transform = CGAffineTransform(scaleX: 0.06, y: 0.06)
        annotationView?.transform = transform
        
        return annotationView
    }
    
    
    
//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//        let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "annotation")
//        annotationView.image = UIImage(named: (records[0].selectedCategory.icon))
//        //annotationView.image = UIImage(named: (records[i].selectedCategory.icon))
//        let transform = CGAffineTransform(scaleX: 0.06, y: 0.06)
//        annotationView.transform = transform
//        return annotationView
//    }
}
