//
//  PieChartController.swift
//  Where is my money
//
//  Created by Mikhail Andreev on 18/05/2018.
//  Copyright © 2018 Mikhail Andreev. All rights reserved.
//

import UIKit
import RealmSwift
import Charts

class PieChartController: UIViewController {
    
    @IBAction func recordTypePicker(_ sender: UISegmentedControl) {
    }
    @IBOutlet weak var pieChartView: PieChartView!
    @IBAction func firstDateButton(_ sender: Any) {
    }
    @IBOutlet weak var firstDateButtonOutlet: UIButton!
    @IBAction func secondDateButton(_ sender: Any) {
    }
    @IBOutlet weak var secondDateButtonOutlet: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    let realm = try! Realm()
    lazy var expenseRecords: Results<RecordModel> = { self.realm.objects(RecordModel.self).filter("isIncomeType == false") }()
    lazy var incomeRecords: Results<RecordModel> = { self.realm.objects(RecordModel.self).filter("isIncomeType == true") }()
    
    public func expenseRecordsSum<T: AddableType>(ofProperty property: String) -> T? {
        return expenseRecords.sum(ofProperty: property)
    }
    
    public func incomeRecordsSum<T: AddableType>(ofProperty property: String) -> T? {
        return incomeRecords.sum(ofProperty: property)
    }
    
    func updateChartData() {
        let expenseDataEntry = PieChartDataEntry(value: expenseRecordsSum(ofProperty: "total")!)
        expenseDataEntry.label = "Расходы"
        expenseDataEntry.accessibilityValue = nil
        let incomeDataEntry = PieChartDataEntry(value: incomeRecordsSum(ofProperty: "total")!)
        incomeDataEntry.label = "Доходы"
        let dataEntries = [expenseDataEntry, incomeDataEntry]
        let chartDataSet = PieChartDataSet(values: dataEntries, label: nil)
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 1
        formatter.multiplier = 1.0
        pieChartView.chartDescription?.text = ""
        pieChartView.usePercentValuesEnabled = true
        pieChartView.legend.horizontalAlignment = .center
        
        let chartData = PieChartData(dataSet: chartDataSet)
        chartData.setValueFormatter(DefaultValueFormatter(formatter:formatter))
        chartData.setValueFont(UIFont(name: "AppleSDGothicNeo-Light", size: 14.0)!)
        let legend = pieChartView.legend
        legend.font = UIFont(name: "AppleSDGothicNeo-Light", size: 24.0)!
        legend.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        pieChartView.holeColor = nil
        let colors = [#colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1), #colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1)]
        chartDataSet.colors = colors
        pieChartView.data = chartData
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.hidesBarsOnSwipe = false
        navigationController?.setNavigationBarHidden(false, animated: true)
        self.title = "Анализ"
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        pieChartView.chartDescription?.text = ""
        updateChartData()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
