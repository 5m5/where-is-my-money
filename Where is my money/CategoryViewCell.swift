//
//  CategoryViewCell.swift
//  Where is my money
//
//  Created by Mikhail Andreev on 26/05/2018.
//  Copyright © 2018 Mikhail Andreev. All rights reserved.
//

import UIKit

class CategoryViewCell: UICollectionViewCell {
    @IBOutlet weak var icon: UIImageView!
}
