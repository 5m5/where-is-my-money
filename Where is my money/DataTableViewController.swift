//
//  DataTableViewController.swift
//  Where is my money
//
//  Created by Mikhail Andreev on 30/04/2018.
//  Copyright © 2018 Mikhail Andreev. All rights reserved.
//

import UIKit
import RealmSwift

class DataTableViewController: UITableViewController {
    
    let realm = try! Realm()
    lazy var records: Results<RecordModel> = { self.realm.objects(RecordModel.self).sorted(byKeyPath: "date", ascending: false) }()
    lazy var bills: Results<Bill> = { self.realm.objects(Bill.self) }()
    var selectedBill: Bill!

    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    @IBOutlet weak var billTitle: UINavigationItem!
    @IBAction func selectBill(_ sender: Any) {
        alertSelectBill()
    }
    
    func alertSelectBill() {
        let alertController = UIAlertController(title: nil, message: "Выберите счет", preferredStyle: .alert)
        
        for bill in bills {
            let selectAction = UIAlertAction(title: bill.name, style: .default) {
                UIAlertAction in
                self.billTitle.title = bill.name
                self.selectedBill = bill
                self.tableView.reloadData()
            }
            alertController.addAction(selectAction)
        }
        let cancel = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Add a background view to the table view
        let backgroundImage = UIImage(named: "Background")
        let imageView = UIImageView(image: backgroundImage)
        self.tableView.backgroundView = imageView
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = imageView.bounds
        imageView.addSubview(blurView)
        
        navigationController?.hidesBarsOnSwipe = false
        navigationController?.setNavigationBarHidden(false, animated: true)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        if selectedBill == nil {
            selectedBill = bills.first
            billTitle.title = selectedBill.name
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        //return records.count
        if selectedBill.name == "Общий счет" {
            return records.count
        }
        return records.filter("selectedBill = %@", selectedBill).count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "DataCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! DataTableViewCell
        
        cell.backgroundColor = .clear

        cell.backgroundColorView.layer.cornerRadius = cell.backgroundColorView.frame.height / 2
        
        var recordsData: RecordModel
        if selectedBill.name == "Общий счет" {
            recordsData = records[indexPath.row]
        } else {
            recordsData = records.filter("selectedBill = %@", selectedBill)[indexPath.row]
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        dateFormatter.locale = Locale.init(identifier: "ru_RU")
        
        cell.dateLabel.text = dateFormatter.string(from: recordsData.date)
        if recordsData.selectedCategory != nil {
            cell.categoryLabel.text = recordsData.selectedCategory.name
            cell.categoryIcon.image = UIImage(named: recordsData.selectedCategory.icon)
        }
        if (recordsData.total.truncatingRemainder(dividingBy: 1)) == 0 {
            cell.totalLabel.text = String(format: "%.0f", recordsData.total) + " ₽"
        }
        else {
            cell.totalLabel.text = String(recordsData.total) + " ₽"
        }
        cell.descriptionLabel.text = recordsData.commentary
        
        //cell.categoryIcon.layer.cornerRadius = cell.categoryIcon.frame.height / 2
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Удалить") { (action, indexPath) in
            try! self.realm.write {
                self.realm.delete(self.records[indexPath.row])
            }
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        }
        return [delete]
    }
    
    @IBAction func unwindFromSegue(_ sender: UIStoryboardSegue) {}
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let segueIdenitfier = "addRecord"
        if segue.identifier == segueIdenitfier {
            let dvc = segue.destination as! RecordViewController
            dvc.selectedBill = selectedBill
        }
    }
}
