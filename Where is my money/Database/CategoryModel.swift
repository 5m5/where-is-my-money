//
//  CategoryModel.swift
//  Where is my money
//
//  Created by Mikhail Andreev on 12/05/2018.
//  Copyright © 2018 Mikhail Andreev. All rights reserved.
//

import UIKit
import RealmSwift

class CategoryModel: Object {
    @objc dynamic var name = ""
    @objc dynamic var icon = ""
    @objc dynamic var isIncomeType = false
}
