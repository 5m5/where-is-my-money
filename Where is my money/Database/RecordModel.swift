//
//  RecordModel.swift
//  Where is my money
//
//  Created by Mikhail Andreev on 12/05/2018.
//  Copyright © 2018 Mikhail Andreev. All rights reserved.
//

import UIKit
import RealmSwift

class RecordModel: Object {
    @objc dynamic var total: Double = 0.0
    @objc dynamic var commentary: String? = nil
    //@objc dynamic var photo: UIImage? = nil
    @objc dynamic var latitude = 0.0
    @objc dynamic var longitude = 0.0
    @objc dynamic var date = Date()
    @objc dynamic var selectedCategory: CategoryModel!
    @objc dynamic var selectedBill: Bill!
    @objc dynamic var isIncomeType = false
}
