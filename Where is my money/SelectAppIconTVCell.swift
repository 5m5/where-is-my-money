//
//  SelectAppIconTVCell.swift
//  Where is my money
//
//  Created by Mikhail Andreev on 21/05/2018.
//  Copyright © 2018 Mikhail Andreev. All rights reserved.
//

import UIKit

class SelectAppIconTVCell: UITableViewCell {
    
    @IBOutlet weak var backgroundColorView: UIView!
    @IBOutlet weak var appIcon: UIImageView!
    @IBOutlet weak var appIconName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
