//
//  AddNewCategoryVC.swift
//  Where is my money
//
//  Created by Mikhail Andreev on 22/05/2018.
//  Copyright © 2018 Mikhail Andreev. All rights reserved.
//

import UIKit

class AddNewCategoryVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var count = 0
    
    let expenseIcons = ["001-eye-glasses", "002-helicopter", "003-vest-suit", "004-handbag", "005-restaurant", "006-bar", "007-casino", "008-atm", "009-bowl", "010-crown", "011-shopping-bag", "012-yatch", "013-watch", "014-gift", "015-mirror", "016-shoes", "017-castle", "018-island", "019-picture", "020-golf", "021-gem", "022-resort", "023-red-carpet", "024-pendant", "025-pearl", "026-boot", "027-money", "028-canoe", "029-swimming-pool", "030-chandeliers", "031-party", "032-cigar", "033-ring", "034-earings", "035-hammock", "036-perfume", "037-dubai", "038-pipe", "039-luggage", "040-clutch", "041-cabriolet", "042-treasure", "043-villa", "044-ingot", "045-armchair", "046-cocktail", "047-necklace", "048-wine", "049-sofa", "050-diamond"]
    
    let incomeIcons = ["001-wallet", "002-trophy", "003-teamwork", "004-target", "005-success", "006-solution", "007-security", "008-process", "009-presentation", "010-piggy-bank", "011-pie-chart", "012-money", "013-money-bag", "014-marketing", "015-line-chart", "016-investment", "017-interview", "018-idea", "019-folder", "020-exchange", "021-ecommerce", "022-document", "023-customer-service", "024-banknote", "025-office-chair", "026-certificate", "027-cashier-machine", "028-card", "029-calendar", "030-calculator", "031-businesswoman", "032-handshake", "033-businessman", "034-briefcase", "035-bar-chart", "036-bank"]
    
    @IBOutlet weak var categoryName: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBAction func categoryTypePicker(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            count = incomeIcons.count
        } else if sender.selectedSegmentIndex == 1 {
            count = expenseIcons.count
        }
    }
    
    override func viewDidLoad() {
        self.count = incomeIcons.count
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.count
        //return incomeIcons.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "CollectionViewCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! CategoryViewCell
        cell.icon.image = UIImage(named: incomeIcons[indexPath.row])
        return cell
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if indexPath.row == 0 {
//            let controller = self.storyboard?.instantiateViewController(withIdentifier: "DataTableViewController") as! DataTableViewController
//            self.navigationController?.pushViewController(controller, animated: true)
//        } else if indexPath.row == 1 {
//            let controller = self.storyboard?.instantiateViewController(withIdentifier: "PieChartController") as! PieChartController
//            self.navigationController?.pushViewController(controller, animated: true)
//        } else if indexPath.row == 2 {
//            let controller = self.storyboard?.instantiateViewController(withIdentifier: "PlanningViewController") as! PlanningViewController
//            self.navigationController?.pushViewController(controller, animated: true)
//        } else if indexPath.row == 3 {
//            let controller = self.storyboard?.instantiateViewController(withIdentifier: "MoneyboxViewController") as! MoneyboxViewController
//            self.navigationController?.pushViewController(controller, animated: true)
//        } else if indexPath.row == 4 {
//            let controller = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
//            self.navigationController?.pushViewController(controller, animated: true)
//        } else if indexPath.row == 5 {
//            let controller = self.storyboard?.instantiateViewController(withIdentifier: "SettingsTableViewController") as! SettingsTableViewController
//            self.navigationController?.pushViewController(controller, animated: true)
//        }
//    }
}
