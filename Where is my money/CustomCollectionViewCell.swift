//
//  CustomCollectionViewCell.swift
//  Where is my money
//
//  Created by Mikhail Andreev on 25/05/2018.
//  Copyright © 2018 Mikhail Andreev. All rights reserved.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var labelCell: UILabel!
}
