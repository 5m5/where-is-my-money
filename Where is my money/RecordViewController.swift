//
//  RecordViewController.swift
//  Where is my money
//
//  Created by Mikhail Andreev on 10/05/2018.
//  Copyright © 2018 Mikhail Andreev. All rights reserved.
//

import UIKit
import RealmSwift
import MapKit

class RecordViewController: UIViewController {
    
    var latitude: CLLocationDegrees!
    var longitude: CLLocationDegrees!
    
    let realm = try! Realm()
    lazy var category: Results<CategoryModel> = { self.realm.objects(CategoryModel.self) }()
    var selectedCategory: CategoryModel!
    var selectedBill: Bill!
    
    func saveIncomeData() {
        try! realm.write() {
            let newRecord = RecordModel()
            
            if totalTextField.text != "" {
                newRecord.total = Double(totalTextField.text!)!
                newRecord.commentary = descriptionTextField.text!
                newRecord.date = datePicker.date
                newRecord.selectedCategory = selectedCategory
                
                if selectedCategory == nil {
                    newRecord.selectedCategory = category.first
                } else {
                    newRecord.selectedCategory = selectedCategory
                }
                
                if selectedCategory.isIncomeType == true {
                    newRecord.isIncomeType = true
                } else {
                    newRecord.isIncomeType = false
                }
                
                if latitude != nil {
                    newRecord.latitude = self.latitude
                    newRecord.longitude = self.longitude
                }
                
                newRecord.selectedBill = selectedBill
                
                self.realm.add(newRecord)
            }
        }
    }
    
    @IBAction func recordTypePicker(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            if (selectedCategory == nil || selectedCategory.isIncomeType == false) {
                selectedCategory = category.filter("isIncomeType == true").first
            }
            categoryLabel.text = selectedCategory.name
            selectCategoryButtonOutlet.setImage(UIImage(named: selectedCategory.icon), for: .normal)
        } else if sender.selectedSegmentIndex == 1 {
            if (selectedCategory == nil || selectedCategory.isIncomeType == true) {
                selectedCategory = category.filter("isIncomeType == false").first
            }
                categoryLabel.text = selectedCategory.name
                selectCategoryButtonOutlet.setImage(UIImage(named: selectedCategory.icon), for: .normal)
        }
    }
    
    @IBOutlet weak var billTitle: UINavigationItem!
    @IBOutlet weak var totalTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBAction func mapButton(_ sender: UIButton) {
    }
    @IBOutlet weak var mapButtonOutlet: UIButton!
    
    @IBOutlet weak var selectCategoryButtonOutlet: UIButton!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBAction func saveButton(_ sender: UIButton) {
        saveIncomeData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.hidesBarsOnSwipe = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedCategory = category.filter("isIncomeType == true").first
        categoryLabel.text = selectedCategory.name
        selectCategoryButtonOutlet.setImage(UIImage(named: selectedCategory.icon), for: .normal)
        billTitle.title = selectedBill.name
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func unwindFromSegueRecord(_ sender: UIStoryboardSegue) {
        if sender.source is SelectCategoryTVC {
            if let SenderVC = sender.source as? SelectCategoryTVC {
                selectedCategory = SenderVC.selectedCategory
                categoryLabel.text = selectedCategory.name
                selectCategoryButtonOutlet.setImage(UIImage(named: selectedCategory.icon), for: .normal)
            }
        }
    }
    
    @IBAction func unwindFromSegueMapPoint(_ sender: UIStoryboardSegue) {
        if sender.source is AddPlaceVC {
            if let SenderVC = sender.source as? AddPlaceVC {
                latitude = SenderVC.latitude
                longitude = SenderVC.longitude
                mapButtonOutlet.setImage(UIImage(named: "placeholderOk"), for: .normal)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let segueIdenitfier = "selectCategory"
        if segue.identifier == segueIdenitfier {
            let dvc = segue.destination as! SelectCategoryTVC
            dvc.selectedCategory = selectedCategory
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
