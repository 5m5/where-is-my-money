//
//  AppDelegate.swift
//  Where is my money
//
//  Created by Mikhail Andreev on 21/04/2018.
//  Copyright © 2018 Mikhail Andreev. All rights reserved.
//

import UIKit
import RealmSwift
import LocalAuthentication

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    let realm = try! Realm()
    lazy var category: Results<CategoryModel> = { self.realm.objects(CategoryModel.self) }()
    //lazy var categoryIncome: Results<IncomeCategoryModel> = { self.realm.objects(IncomeCategoryModel.self) }()
    lazy var bills: Results<Bill> = { self.realm.objects(Bill.self) }()
    
    func authenticate(){
        let context = LAContext()
        let reason = "We need this to protect your payments." // add your own message explaining why you need this authentication method
        
        var authError: NSError?
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { success, error in
                if success {
                    // User authenticated successfully
                } else {
                    // User did not authenticate successfully
                }
            }
        } else {
            // Handle Error
        }
    }
    
    func populateDefaultBills() {
        if bills.count == 0 {
            try! realm.write() {
                
                let defaultBills = ["Общий счет", "Кредитная карта", "Наличные"]
                
                for bill in defaultBills {
                    let newBill = Bill()
                    newBill.name = bill
                    self.realm.add(newBill)
                }
                
            }
            bills = realm.objects(Bill.self)
        }
    }
    
    func populateDefaultCategories() {
        if category.count == 0 {
            try! realm.write() {
                
                let defaultExpenseCategories = ["Дом": "houseIcon", "Продукты": "foodIcon", "Транспорт": "transportIcon", "Одежда": "clothesIcon", "Здоровье": "healthIcon", "Досуг": "leisureIcon"]
                
                let defaultIncomeCategories = ["Зарплата": "salaryIcon", "Продажа": "saleIcon", "Подарок": "giftIcon", "Выигрыш": "winIcon"]
                
                for (category, icon) in defaultExpenseCategories {
                    let newCategory = CategoryModel()
                    newCategory.name = category
                    newCategory.icon = icon
                    newCategory.isIncomeType = false
                    self.realm.add(newCategory)
                }
                
                for (category, icon) in defaultIncomeCategories {
                    let newCategory = CategoryModel()
                    newCategory.name = category
                    newCategory.icon = icon
                    newCategory.isIncomeType = true
                    self.realm.add(newCategory)
                }
            }
            category = realm.objects(CategoryModel.self)
        }
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UINavigationBar.appearance().tintColor = .white
        if let barFont = UIFont(name: "AppleSDGothicNeo-Light", size: 24) {
            UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white, NSAttributedStringKey.font: barFont]
        }
        
        UITabBar.appearance().tintColor = .white
        //UITabBar.appearance().barTintColor = #colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1)
        
        authenticate()
        populateDefaultCategories()
        populateDefaultBills()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        
    }

    

}

