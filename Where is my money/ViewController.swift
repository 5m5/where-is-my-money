//
//  ViewController.swift
//  Where is my money
//
//  Created by Mikhail Andreev on 24/05/2018.
//  Copyright © 2018 Mikhail Andreev. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    let mainMenu = ["Обзор", "Анализ", "Планирование", "Копилка", "Карта", "Настройки"]
    let mainMenuImages = ["overview", "analytics", "time-is-money", "piggybank", "map", "settings"]
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return mainMenu.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "MainCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! CustomCollectionViewCell
        cell.imageCell.image = UIImage(named: mainMenuImages[indexPath.row])
        cell.labelCell.text = mainMenu[indexPath.row].capitalized
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "DataTableViewController") as! DataTableViewController
            self.navigationController?.pushViewController(controller, animated: true)
        } else if indexPath.row == 1 {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "PieChartController") as! PieChartController
            self.navigationController?.pushViewController(controller, animated: true)
        } else if indexPath.row == 2 {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "PlanningViewController") as! PlanningViewController
            self.navigationController?.pushViewController(controller, animated: true)
        } else if indexPath.row == 3 {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "MoneyboxViewController") as! MoneyboxViewController
            self.navigationController?.pushViewController(controller, animated: true)
        } else if indexPath.row == 4 {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            self.navigationController?.pushViewController(controller, animated: true)
        } else if indexPath.row == 5 {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "SettingsTableViewController") as! SettingsTableViewController
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func unwindFromSegueMapToMain(_ sender: UIStoryboardSegue) {}

}
